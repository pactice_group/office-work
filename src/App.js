import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Home from './pages/home/Home'
import styles from './App.module.scss'
import TopBar from './components/topBar/TopBar'
import Header from './components/header/Header'
import Shop from './pages/shop/Shop'
import Products from './pages/products/Products'
import Blog from './pages/blog/Blog'
import Pages from './pages/pages/Pages'
import Contact from './pages/contact/Contact'


const App = () => {
  return (
    <div className={styles.appContainer}>
      <TopBar />
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/shop' element={<Shop />} />
        <Route path='/products' element={<Products />} />
        <Route path='/blog' element={<Blog />} />
        <Route path='/pages' element={<Pages />} />
        <Route path='/contact' element={<Contact />} />
      </Routes>
    </div>
  )
}

export default App