import React from 'react'
import styles from './Home.module.scss'
import { Box, Grid, Typography } from '@mui/material'
import Carousel from '../../components/carousel/Carousel';
import CommonCard from '../../components/card/CommonCard';

const Home = () => {
    return (
        <Grid className={styles.homeContainer}>
            <Carousel />
            <Box className={styles.homePageHeader}>
                <Typography variant='h5' fontWeight={600} fontSize={15} align='center'>FEATURED IN</Typography>
                <Typography variant='h2' fontWeight={500} fontSize={35} align='center'>Men Shop</Typography>
            </Box>
            <Box>
                <CommonCard />
            </Box>
        </Grid>
    )
}

export default Home