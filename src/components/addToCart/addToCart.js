import React from 'react'
import styles from './addToCart.module.scss'
import { Box, Grid, Typography } from '@mui/material'
import LocalMallOutlinedIcon from '@mui/icons-material/LocalMallOutlined';


const AddToCart = () => {
    return (
        <Grid className={styles.addToCartContainer}>
            <LocalMallOutlinedIcon />
            <Box>
                <Typography>0</Typography>
            </Box>

        </Grid>
    )
}

export default AddToCart;