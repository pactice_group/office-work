import React from 'react'
import styles from './CommonButton.module.scss'
import { Button, Grid } from '@mui/material'

const CommonButton = ({ name, handleClick }) => {
    return (
        <Grid className={styles.commonButtonContainer}>
            <Button onClick={handleClick}>{name}</Button>
        </Grid>
    )
}

export default CommonButton