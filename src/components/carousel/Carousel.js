import React from 'react'
import styles from './Carousel.module.scss'
import { Grid } from '@mui/material'
import Banner from '../../components/banner/Banner'
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";

const Carousel = () => {

    return (
        <Grid className={styles.carouselContainer}>
            <AliceCarousel autoPlay={true} infinite={true} autoPlayInterval={3000}>
                <Banner
                    bannerHeading={"a color for every body"}
                    bannerSubHeading={"Open Doors To a World of Fashion"}
                    actionName={"Shop Now"}
                    bannerAction={(() => alert("fbjk"))}
                />
                <Banner
                    bannerHeading={"SHOP YOUR OUTFIT STYLE"}
                    bannerSubHeading={"Tell Your copaign through images "}
                    actionName={"Shop Now"}
                    bannerAction={(() => alert("fbjk"))}
                />
                <Banner
                    bannerHeading={"a color for every body"}
                    bannerSubHeading={"Open Doors To a World of Fashion"}
                    actionName={"Shop Now"}
                    bannerAction={(() => alert("fbjk"))}
                />
            </AliceCarousel>
        </Grid>
    )
}

export default Carousel