import React from 'react'
import styles from './Banner.module.scss'
import { Grid, Typography } from '@mui/material'
import CommonButton from '../CommonButton/CommonButton'

const Banner = ({ bannerHeading, bannerSubHeading, actionName, bannerAction }) => {
    return (
        <Grid className={styles.bannerContainer}>
            <Typography variant='h2' fontWeight={600} >{bannerHeading}</Typography>
            <Typography>{bannerSubHeading}</Typography>
            <CommonButton
                name={actionName}
                handleClick={bannerAction}
            />
        </Grid>
    )
}

export default Banner;