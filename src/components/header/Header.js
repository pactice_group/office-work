import React from 'react'
import styles from './Header.module.scss'
import { Grid, Typography } from '@mui/material'
import data from './data.json'
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import brand from '../../images/brand.webp'
import { useNavigate } from 'react-router-dom';
import AddToCart from '../addToCart/addToCart';

const Header = () => {
    const navigation = useNavigate()
    return (
        <Grid className={styles.headerContainer}>
            <Grid container className={styles.navLink}>
                {data.map((item) => {
                    return (
                        <Typography onClick={(() => navigation(item.link))} key={item.id}>
                            {item.label}
                        </Typography>
                    )
                })}
                <img src={brand} alt='buxton' />
            </Grid>
            <Grid className={styles.navIcons}>
                <PersonOutlineOutlinedIcon />
                <SearchOutlinedIcon />
                <AddToCart />
            </Grid>
        </Grid>
    )
}

export default Header;