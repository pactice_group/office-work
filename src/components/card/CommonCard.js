import React from 'react'
import styles from './CommonCard.module.scss'
import { Card, CardContent, CardMedia, Grid, Typography } from '@mui/material'
import img from '../../images/image_1.jpg'
import data from './data.json'

const CommonCard = () => {
    console.log(data)
    return (
        <Grid container className={styles.cardContainer}>
            {data.map((item) => {
                return (
                    <Grid item md={3}>
                        <Card className={styles.cardContainer}>
                            <CardMedia>
                                <img src={item.img} alt='img' />
                            </CardMedia>
                            <CardContent>
                                <Typography>{item.lable}</Typography>
                                <Typography>{item.heading}</Typography>
                                <Typography>{item.price}</Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                )
            })}
        </Grid>
    )
}

export default CommonCard