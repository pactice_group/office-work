import React from 'react'
import styles from './TopBar.module.scss'
import { Grid, Typography } from '@mui/material'
import data from './data.json'

const TopBar = () => {
    return (
        <Grid container className={styles.topBarContainer}>
            {data.map((item) => {
                return (
                    <Grid key={item.id} item md={4}>
                        <Typography >
                            {item.label} <span>{item.label2}</span>
                        </Typography>
                    </Grid>
                )
            })}
        </Grid>
    )
}

export default TopBar